import styled from "styled-components";
import headerbg from "../assets/headerbg.png";


export const TopBanner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
  width: 100%;
  height: 350px;
  background-image: url(${headerbg});
  @media (max-width: 300px){
    flex-flow: row nowrap;
    width: 100%;
  }
`;
export const Title = styled.div`
  text-align: center;
`;
export const Name = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  color: white;
  flex-wrap: wrap;
  text-shadow: 2px 2px #000;
  font-size: 35px;
  padding: 20px;
`;
export const SubHeader = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  color: white;
  text-shadow: 2px 2px #000;
  font-size: 25px;
`;
export const Body = styled.div`
  height: 800px;
  width: 100%;
  background-color: #484848;
`;
export const Content = styled.div`
  display: grid;
  grid-template-rows: repeat(2, 1fr);
`;
export const RowOne = styled.div`
  background-color: #e8eaed; 
  border-radius: 5px; 
  height: 300px;
`;
export const IconRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`;