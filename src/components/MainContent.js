import React, { Component } from 'react';
import { 
  Body, 
  Name, 
  Content,
  RowOne,
  IconRow 
} from '../styles/styled';
import "../index.css";
import html from "../assets/icons/html.svg";
import css from "../assets/icons/css.svg";
import javascript from "../assets/icons/javascript.svg";
import react from "../assets/icons/react.svg";



class MainContent extends Component {
  render() {
    return (
      <div>
        <Body>
          <Content>
            <RowOne>
              <Name>Skills</Name>
              <IconRow>
                <img className="icon" src={html} alt="html" />
                <img className="icon" src={css} alt="css" />
                <img className="icon" src={javascript} alt="javascript" />
                <img className="icon" src={react} alt="react" />
              </IconRow>
              <p style={{fontSize: "30px", textAlign: "center"}}>{`Under Construction...`}</p>            
            </RowOne>
          </Content>
        </Body>
      </div>
    )
  }
}

export default MainContent;
