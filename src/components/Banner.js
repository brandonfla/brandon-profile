import React, { Component } from "react";
import { 
  TopBanner,
  Name,
  SubHeader,
  Title
 } from "../styles/styled.js";


class Banner extends Component {
  render() {
    return (
      <div>
        <TopBanner>
          <Title>
            <Name>Brandon Fitzgerald</Name>
            <SubHeader>Front-End Web Developer</SubHeader>
          </Title>
        </TopBanner>
      </div>
    )
  }
}

export default Banner;
