import React from 'react';
import './App.css'
import Banner from './components/Banner';
import MainContent from './components/MainContent';

function App() {
  return (
    <div>
      <Banner />
      <MainContent />
    </div>
  );
}

export default App;
