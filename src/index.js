import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import linkedin from "./assets/icons/linkedin.svg";
import github from "./assets/icons/github.svg";
import gitlab from "./assets/icons/gitlab.svg";
import resume from "./assets/icons/resume.svg";
import App from './App';
import * as serviceWorker from './serviceWorker';

const routing = (
  <div className="navbar">
  <Router>
    <div>
      <div>
        <ul className="nav">
          <li className="link">
            <a href="https://www.linkedin.com/in/brandonfla/" target="_blank" rel="noopener noreferrer">
              <img src={linkedin} alt="linkedin" />
            </a>
          </li>
          <li className="link">
            <a href="https://www.github.com/brandonfla" target="_blank" rel="noopener noreferrer">
              <img src={github} alt="github" />
            </a>
          </li>
          <li className="link">
            <a href="https://www.gitlab.com/brandonfladev" target="_blank" rel="noopener noreferrer">
              <img src={gitlab} alt="gitlab" />
            </a>
          </li>
          <li className="link">
            <a href="/doc/resume.html">
              <img src={resume} alt="resume" />
            </a>
          </li>
        </ul>
      </div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/" component={App} />
        <Route exact path="/" component={App} />
        <Route path="/" component={App} />
      </Switch>
    </div>
  </Router>
  </div>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
