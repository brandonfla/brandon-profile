## Personal / Professional site for Brandon Fitzgerald. Find it [here.](https://brandonfladev.cc)
---
#### This site is currently built using:  

* React (This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app))
* React-router
* React-Dom
* Styled-Components
* Material-UI
* [DEVICON](https://konpa.github.io/devicon/)
* Resume is standard HTML and CSS
---
##### Currently no back-end needed
---
#### WIP/ TODOs:

* Add text below Nav Icons
* Add content to page body
* Add new projects
* Create jsconfig.json ``` { "compilerOptions": { "baseUrl": "src" } } ``` for clean imports
* More to be added to this list...
---
#### Maybes:

* Integrate Bulma
* Integrate React-Image
* Integrate React-Icons
